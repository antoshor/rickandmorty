//
//  DetailPresenterProtocol.swift
//  Rick&Morty
//
//  Created by Mac Admin on 17.08.2023.
//

import Foundation

protocol DetailPresenterProtocol: AnyObject {
   
    func initialLoad()
    
    func didLoadData(_ data: DetailSnapshotModel)
}
