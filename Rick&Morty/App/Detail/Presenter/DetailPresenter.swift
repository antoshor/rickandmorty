//
//  DetailPresenter.swift
//  Rick&Morty
//
//  Created by Mac Admin on 17.08.2023.
//

import Foundation

final class DetailPresenter {
    
    // MARK: - Public Properties
    weak var view: DetailViewProtocol?
    var interactor: DetailInteractorProtocol
    var router: DetailRouterProtocol
    var snapshotDelegate: DetailSnapshot?
    
    // MARK: - Private Properties
    private var profile: Profile?
    private var info: Information?
    private var origin: Origin?
    private var episodes = [Episode]()
    
    // MARK: - Initializers
    init(interactor: DetailInteractorProtocol, router: DetailRouterProtocol ) {
        self.interactor = interactor
        self.router = router
    }
}

extension DetailPresenter: DetailPresenterProtocol {
    
    func initialLoad() {
        interactor.loadData()
    }
    
    func didLoadData(_ data: DetailSnapshotModel) {
        guard let snapshot = snapshotDelegate?.reloadData(with: data) else {
            return
        }
        view?.showUI(snapshot: snapshot)
    }
}







