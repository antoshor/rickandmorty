//
//  DetailSnapshot.swift
//  Rick&Morty
//
//  Created by Mac Admin on 19.08.2023.
//

import Foundation
import UIKit

protocol DetailSnapshotProtocol {
    
    associatedtype Section: Hashable
    associatedtype Row: Hashable
    associatedtype DiffableData
    
    func reloadData(with data: DiffableData) -> NSDiffableDataSourceSnapshot<Section, Row>
}

final class DetailSnapshot: DetailSnapshotProtocol {
    typealias DiffableData = DetailSnapshotModel
    typealias Section = DetailSection
    typealias Row = DetailRow
    
    func reloadData(with data: DetailSnapshotModel ) -> NSDiffableDataSourceSnapshot<DetailSection, DetailRow> {
        var snapshot = NSDiffableDataSourceSnapshot<DetailSection, DetailRow>()
        
        snapshot.appendSections([.profile])
        if let profile = data.profile {
            snapshot.appendItems([.profileInfo(profile)], toSection: .profile)
        }
        
        snapshot.appendSections([.info])
        if let info = data.info {
            snapshot.appendItems([.infoInfo(info)], toSection: .info)
            
        }
        
        snapshot.appendSections([.origin])
        if let origin = data.origin {
            snapshot.appendItems([.originInfo(origin)], toSection: .origin)
          
        }

        snapshot.appendSections([.episodes])
        if let episodes = data.episodes {
            episodes.forEach { episode in
                snapshot.appendItems([.episodesInfo(episode)], toSection: .episodes)
            }
        }
        
        return snapshot
    }
}
