//
//  DetailEpisodeHeader.swift
//  Rick&Morty
//
//  Created by Mac Admin on 21.08.2023.
//
import Foundation
import UIKit

// MARK: - CategoriesSectionHeader
class DetailHeader: UICollectionReusableView {
    
    static let reuserID = "DetailHeader"
    
    // MARK: - Public Properties
    lazy var titleLabel: UILabel = {
        let frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        let label = CustomLable(frame: frame, textColor: .standartWhite, fontName: .gilroySemiBold, fontSize: 17, textAlignment: .left, numberOfLines: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.01568627451, green: 0.04705882353, blue: 0.1176470588, alpha: 1)
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private Methods
    private func setupConstraints() {
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(titleLabel)
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16)
        ])
    }
}
