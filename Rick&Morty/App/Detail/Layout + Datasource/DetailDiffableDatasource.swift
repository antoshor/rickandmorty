//
//  DetailDiffableDatasource.swift
//  Rick&Morty
//
//  Created by Mac Admin on 19.08.2023.
//

import Foundation
import UIKit

// MARK: - DataSource
protocol DetailDiffableDataSourceProtocol {
    
    init(collectionView: UICollectionView)
    
    var dataSource: UICollectionViewDiffableDataSource<DetailSection, DetailRow>? { get }
}

// MARK: - MainDataSource
final class DetailDiffableDataSource: DetailDiffableDataSourceProtocol {
    
    // MARK: - Public Properties
    var dataSource: UICollectionViewDiffableDataSource<DetailSection, DetailRow>?
    
    // MARK: - Initializers
    required init(collectionView: UICollectionView) {
        configureDataSource(collectionView: collectionView)
    }
    
    // MARK: - Private Methods
    private func configureDataSource(collectionView: UICollectionView) {
        dataSource = UICollectionViewDiffableDataSource<DetailSection, DetailRow>(collectionView: collectionView, cellProvider: { (collectionView, indexPath, item) -> UICollectionViewCell? in
            switch item {
            case .profileInfo(let profile):
                let cell = self.createCell(cellType: ProfileCollectionViewCell.self, indexPath: indexPath, collectionView: collectionView)
                cell.configure(with: profile)
                return cell
                
            case .infoInfo(let info):
                let cell = self.createCell(cellType: InfoCollectionViewCell.self, indexPath: indexPath, collectionView: collectionView)
                cell.configure(with: info)
                return cell
                
            case .originInfo(let origin):
                let cell = self.createCell(cellType: OriginCollectionViewCell.self, indexPath: indexPath, collectionView: collectionView)
                cell.configure(with: origin)
                return cell
                
            case .episodesInfo(let episode):
                let cell = self.createCell(cellType: EpisodesCollectionViewCell.self, indexPath: indexPath, collectionView: collectionView)
                cell.configure(with: episode)
                return cell
            }
        })
        
        let headerRegistration = UICollectionView.SupplementaryRegistration
        <DetailHeader>(elementKind: UICollectionView.elementKindSectionHeader) { (headerView, elementKind, indexPath) in
            guard let header = DetailSection(rawValue: indexPath.section)?.header else {
                return
            }
            headerView.titleLabel.text = header
        }
        
        dataSource?.supplementaryViewProvider = {
            (collectionView, elementKind, indexPath) -> UICollectionReusableView? in
            
            guard let section = DetailSection(rawValue: indexPath.section) else {
                return UICollectionReusableView()
            }
            
            switch section {
            case .profile:
                return nil
            default:
                return collectionView.dequeueConfiguredReusableSupplementary(
                    using: headerRegistration, for: indexPath)
            }
        }
        
    }
    
    private func createCell<T: CollectionViewCellsProtocol>(cellType: T.Type, indexPath: IndexPath, collectionView: UICollectionView) -> T {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellType.reuseID, for: indexPath) as? T else {
            fatalError("Error \(cellType)")
        }
        return cell
    }
}
