//
//  DetailCompositionalLayout.swift
//  Rick&Morty
//
//  Created by Mac Admin on 19.08.2023.
//

import Foundation
import UIKit

final class DetailCompositionalLayout: CompositionalLayoutProtocol {
    
    static func createLayuot() -> UICollectionViewLayout {
        let layout = UICollectionViewCompositionalLayout  { (sectionIndex, layoutEnvironment) -> NSCollectionLayoutSection? in
         
            guard let sectionKind = DetailSection(rawValue: sectionIndex) else {
                return nil
            }
            
            switch sectionKind {
            case .profile:
                return self.getSection(groupHeigth: sectionKind.sectionSize, whitHedear: false)
            case .info:
                return self.getSection(groupHeigth: sectionKind.sectionSize, whitHedear: true)
            case .origin:
                return self.getSection(groupHeigth: sectionKind.sectionSize, whitHedear: true)
            case .episodes:
                return self.getSection(groupHeigth: sectionKind.sectionSize, interGroupSpacing: 16, whitHedear: true)
            }
        }
        return layout
    }
    
    // MARK: - Private Methods
    private static func getSection(verticalSpacing: CGFloat = 8, edgeSpacing: CGFloat = 24, groupHeigth: CGFloat, interGroupSpacing: CGFloat = 0, whitHedear: Bool) -> NSCollectionLayoutSection {
        
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .estimated(groupHeigth))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
        
        let section = NSCollectionLayoutSection(group: group)
        section.interGroupSpacing = interGroupSpacing
        section.contentInsets = .init(top: 0, leading: edgeSpacing, bottom: verticalSpacing, trailing: edgeSpacing)

        if whitHedear {
            let header = DetailCompositionalLayout.createSectionHeader()
            section.boundarySupplementaryItems = [header]
        }
        
        return section
    }   
    
    private static func createSectionHeader() -> NSCollectionLayoutBoundarySupplementaryItem {
        let layoutSectionHeaderSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .estimated(25))
        let layoutSectionHeader = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: layoutSectionHeaderSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .top)
       
        layoutSectionHeader.pinToVisibleBounds = true
        return layoutSectionHeader
    }
}

