//
//  DetailModuleBuilder.swift
//  Rick&Morty
//
//  Created by Mac Admin on 17.08.2023.
//

import Foundation
import UIKit

final class DetailModuleBuilder {
    static func build(info: DetailModel) -> DetailViewController {
        let interactor = DetailInteractor(dataInput: info)
        let router = DetailRouter()
        
        let viewController = DetailViewController()
        
        let presenter = DetailPresenter(interactor: interactor, router: router)
        presenter.snapshotDelegate = DetailSnapshot()
        presenter.view = viewController
        
        interactor.presenter = presenter
        router.presenter = presenter
        viewController.presenter = presenter
        
        return viewController
    }
}
