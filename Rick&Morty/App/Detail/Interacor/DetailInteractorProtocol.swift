//
//  DetailInteractorProtocol.swift
//  Rick&Morty
//
//  Created by Mac Admin on 17.08.2023.
//

import Foundation

protocol DetailInteractorProtocol: AnyObject {
 
    func loadData()
}
