//
//  DetailInteractor.swift
//  Rick&Morty
//
//  Created by Mac Admin on 17.08.2023.
//

import Foundation

final class DetailInteractor {
    // MARK: - Public Properties
    weak var presenter: DetailPresenterProtocol?
    
    // MARK: - Private Properties
    private var dataInput: DetailModel
    private var dataOutput = DetailSnapshotModel(profile: nil, info: nil, origin: nil, episodes: nil)
    private let group = DispatchGroup()
    
    // MARK: - Initializers
    init(dataInput: DetailModel) {
        self.dataInput = dataInput
    }
    
    // MARK: - Private Methods
    private func loadEpisodes() {
        let queue = DispatchQueue(label: "interactor.queue")
        dataInput.episodeUrl.forEach { url in
            group.enter()
            queue.async {
                NetworkService.shared.loadData(url: url) { (episode: Result<Episode, LoadingError>) in
                    switch episode {
                    case .success(let item):
                        self.dataOutput.episodes = (self.dataOutput.episodes ?? []) + [item]
                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                    self.group.leave()
                }
            }
        }
    }
    
    private func loadProfile() {
        let profile = Profile(image: dataInput.image, name: dataInput.name, status: dataInput.status)
        dataOutput.profile = profile
    }
    
    private func loadInfo() {
        let info = Information(species: dataInput.species, type: dataInput.type, gender: dataInput.gender)
        dataOutput.info = info
    }
    
    private func loadOrigin() {
        group.enter()
        NetworkService.shared.loadData(url: dataInput.originUrl) { (origin: Result<Origin, LoadingError>) in
            switch origin {
            case .success(let origin):
                self.dataOutput.origin = origin
            case .failure(let error):
                self.dataOutput.origin = Origin(name: "None", type: "None")
                print(error.localizedDescription)
            }
            self.group.leave()
        }
    }
}

// MARK: - DetailInteractorProtocol
extension DetailInteractor: DetailInteractorProtocol {
    func loadData() {
        loadProfile()
        loadInfo()
        loadOrigin()
        loadEpisodes()
        
        group.notify(queue: .main) {
            self.presenter?.didLoadData(self.dataOutput)
        }
    }
}
