//
//  DetailEnums.swift
//  Rick&Morty
//
//  Created by Mac Admin on 19.08.2023.
//

import Foundation
import UIKit

// MARK: - CharactersDifableDataSource
enum DetailSection: Int, CaseIterable {
    case profile
    case info
    case origin
    case episodes
    
    var header: String? {
        switch self {
        case .profile:
            return nil
        case .info:
            return "Info"
        case .origin:
            return "Origin"
        case .episodes:
            return "Episodes"
        }
    }
    
    var sectionSize: CGFloat {
        switch self {
        case .profile:
            return 225
        case .info:
            return 124
        case .origin:
            return 80
        case .episodes:
            return 86
        }
    }
}

enum DetailRow: Hashable {
    case profileInfo(Profile)
    case infoInfo(Information)
    case originInfo(Origin)
    case episodesInfo(Episode)
}
