//
//  DetailModel.swift
//  Rick&Morty
//
//  Created by Mac Admin on 19.08.2023.
//

import Foundation

// MARK: - DetailModel
struct DetailModel: Codable {
    let name: String
    let status: String
    let species: String
    let type: String
    let gender: String
    let image: String
    let originUrl: String
    let episodeUrl: [String]
}

// MARK: - DetailSnapshotModel
struct DetailSnapshotModel {
    var profile: Profile?
    var info: Information?
    var origin: Origin?
    var episodes: [Episode]?
}

// MARK: - Profile
struct Profile: Hashable {
    let image: String
    let name: String
    let status: String
}

// MARK: - Info
struct Information: Hashable {
    let species: String
    let type: String
    let gender: String
}

// MARK: - Origin
struct Origin: Codable, Hashable {
    let name: String
    let type: String
}

// MARK: - Episode
struct Episode: Codable, Hashable {
    let id: Int
    let name, airDate, episode: String
    let characters: [String]
    let url: String
    let created: String

    enum CodingKeys: String, CodingKey {
        case id, name
        case airDate = "air_date"
        case episode, characters, url, created
    }
}
