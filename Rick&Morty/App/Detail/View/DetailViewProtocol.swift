//
//  DetailViewProtocol.swift
//  Rick&Morty
//
//  Created by Mac Admin on 17.08.2023.
//

import Foundation
import UIKit

protocol DetailViewProtocol: AnyObject {
    
    func showUI(snapshot: NSDiffableDataSourceSnapshot<DetailSection, DetailRow>)
}
