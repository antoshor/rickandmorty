//
//  DetailViewController.swift
//  Rick&Morty
//
//  Created by Mac Admin on 17.08.2023.
//

import Foundation
import UIKit

final class DetailViewController: UIViewController, DetailViewProtocol {
    
    // MARK: - Public Properties
    var presenter: DetailPresenterProtocol?
    var collectionViewDataSource: DetailDiffableDataSourceProtocol?
    
    // MARK: - UI Properties
    private lazy var detailCollectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: 100, height: 100), collectionViewLayout: DetailCompositionalLayout.createLayuot())
        collectionView.backgroundColor = #colorLiteral(red: 0.01568627451, green: 0.04705882353, blue: 0.1176470588, alpha: 1)
        
        collectionView.register(DetailHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: DetailHeader.reuserID)
        
        collectionView.register(ProfileCollectionViewCell.self, forCellWithReuseIdentifier: ProfileCollectionViewCell.reuseID)
        collectionView.register(InfoCollectionViewCell.self, forCellWithReuseIdentifier: InfoCollectionViewCell.reuseID)
        collectionView.register(OriginCollectionViewCell.self, forCellWithReuseIdentifier: OriginCollectionViewCell.reuseID)
        collectionView.register(EpisodesCollectionViewCell.self, forCellWithReuseIdentifier: EpisodesCollectionViewCell.reuseID)
        
        return collectionView
    }()
    
    // MARK: - Initializers
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.initialLoad()
        view.backgroundColor = #colorLiteral(red: 0.01568627451, green: 0.04705882353, blue: 0.1176470588, alpha: 1)
        setupDataSource()
        setupConstraints()
    }
    
    // MARK: - Public Methods
    func showUI(snapshot: NSDiffableDataSourceSnapshot<DetailSection, DetailRow>) {
        collectionViewDataSource?.dataSource?.apply(snapshot)
    }
    
    // MARK: - Private Methods
    private func setupConstraints() {
        detailCollectionView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(detailCollectionView)
        let safeArea = view.safeAreaLayoutGuide
          NSLayoutConstraint.activate([
            detailCollectionView.topAnchor.constraint(equalTo: view.topAnchor),
            detailCollectionView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            detailCollectionView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),
            detailCollectionView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor)
        ])
    }
    
    private func setupDataSource() {
        collectionViewDataSource = DetailDiffableDataSource(collectionView: detailCollectionView)
    }
}

