//
//  OriginCollectionViewCell.swift
//  Rick&Morty
//
//  Created by Mac Admin on 18.08.2023.
//

import Foundation
import UIKit

final class OriginCollectionViewCell: UICollectionViewCell, CollectionViewCellsProtocol {
    
    // MARK: - Protocol implementation
    static var reuseID: String = "OriginCollectionViewCell"
    typealias CellType = Origin
    
    // MARK: - Private Properties
    private let queue = DispatchQueue(label: "queue.cell")
    private var checkData: Origin?
    
    // MARK: - UI Properties
    private lazy var indicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .medium)
        indicator.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        indicator.color = .white
        indicator.hidesWhenStopped = true
        return indicator
    }()

    // MARK: - ViewContainer
    private lazy var viewContainer: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.09803921569, green: 0.1098039216, blue: 0.1647058824, alpha: 1)
        view.layer.cornerRadius = 10
        view.addSubview(imageView)
        return view
    }()
    
    // MARK: - ImageView
    private lazy var imageView: UIImageView = {
        let image = UIImageView()
        return image
    }()
    
    // MARK: - Labels
    private lazy var titleLabel: UILabel = {
        let frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        let label = CustomLable(frame: frame, textColor: .standartWhite, fontName: .gilroySemiBold, fontSize: 17, textAlignment: .left, numberOfLines: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var typeLabel: UILabel = {
        let frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        let label = CustomLable(frame: frame, textColor: .green, fontName: .gilroyMedium, fontSize: 13, textAlignment: .left, numberOfLines: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // MARK: - StackViewContainer
    private lazy var stackViewContainer: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 8
        [titleLabel, typeLabel].forEach { stack.addArrangedSubview($0)}
        return stack
    }()
   
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupCellView()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Override Methods
    override func prepareForReuse() {
        imageView.image = nil
        titleLabel.text = nil
        typeLabel.text = nil
    }
    
    // MARK: - Public Methods
    func configure(with cellData: Origin) {
        indicator.startAnimating()
        checkData = cellData
        queue.async {
            guard  self.checkData == cellData else {
                return }
            DispatchQueue.main.async {
                self.indicator.stopAnimating()
                self.imageView.image = UIImage(named: "PlanetImage")
                self.titleLabel.text = cellData.name
                self.typeLabel.text = cellData.type
            }
        }
    }
    
    // MARK: - Private Methods
    private func setupConstraints() {
        indicator.translatesAutoresizingMaskIntoConstraints = false
        addSubview(indicator)
          NSLayoutConstraint.activate([
            indicator.centerXAnchor.constraint(equalTo: centerXAnchor),
            indicator.centerYAnchor.constraint(equalTo: centerYAnchor),
             ])
        
        viewContainer.translatesAutoresizingMaskIntoConstraints = false
        addSubview(viewContainer)
        NSLayoutConstraint.activate([
            viewContainer.heightAnchor.constraint(equalTo: viewContainer.widthAnchor, multiplier: 1),
            
            viewContainer.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            viewContainer.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            viewContainer.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8)
        ])
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.center = viewContainer.center
        NSLayoutConstraint.activate([
            imageView.centerXAnchor.constraint(equalTo: viewContainer.centerXAnchor),
            imageView.centerYAnchor.constraint(equalTo: viewContainer.centerYAnchor),
           
            imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor, multiplier: 1),
            
            imageView.heightAnchor.constraint(equalTo: viewContainer.heightAnchor, multiplier: 0.375),
            imageView.widthAnchor.constraint(equalTo: viewContainer.widthAnchor, multiplier: 0.375)
        ])
        
        stackViewContainer.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stackViewContainer)
        NSLayoutConstraint.activate([
            stackViewContainer.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            stackViewContainer.leadingAnchor.constraint(equalTo: viewContainer.trailingAnchor, constant: 16),
            stackViewContainer.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            stackViewContainer.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16)
        ])
    }
    
    private func setupCellView() {
        contentView.layer.cornerRadius = 10
        contentView.backgroundColor = #colorLiteral(red: 0.1490196078, green: 0.1647058824, blue: 0.2196078431, alpha: 1)
    }
}
