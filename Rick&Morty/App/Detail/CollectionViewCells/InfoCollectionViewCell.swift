//
//  InfoCollectionViewCell.swift
//  Rick&Morty
//
//  Created by Mac Admin on 18.08.2023.
//

import Foundation
import UIKit

final class InfoCollectionViewCell: UICollectionViewCell, CollectionViewCellsProtocol {
    
    // MARK: - Protocol implementation
    static var reuseID: String = "InfoCollectionViewCell"
    typealias CellType = Information
    
    // MARK: - Private Properties
    private let queue = DispatchQueue(label: "queue.cell")
    private var checkData: Information?
    
    // MARK: - UI Properties
    private lazy var indicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .medium)
        indicator.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        indicator.color = .white
        indicator.hidesWhenStopped = true
        return indicator
    }()
    
    // MARK: - Title labels
    private lazy var titleSpeciesLabel: UILabel = {
        let frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        let label = CustomLable(frame: frame, textColor: .lightGray, fontName: .gilroyMedium, fontSize: 16, textAlignment: .left, numberOfLines: 1)
        label.text = "Species:"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var titleTypeLabel: UILabel = {
        let frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        let label = CustomLable(frame: frame, textColor: .lightGray, fontName: .gilroyMedium, fontSize: 16, textAlignment: .left, numberOfLines: 1)
        label.text = "Type:"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var titleGenderLabel: UILabel = {
        let frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        let label = CustomLable(frame: frame, textColor: .lightGray, fontName: .gilroyMedium, fontSize: 16, textAlignment: .left, numberOfLines: 1)
        label.text = "Gender:"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // MARK: - Value labels
    private lazy var speciesLabel: UILabel = {
        let frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        let label = CustomLable(frame: frame, textColor: .standartWhite, fontName: .gilroyMedium, fontSize: 16, textAlignment: .right, numberOfLines: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var typeLabel: UILabel = {
        let frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        let label = CustomLable(frame: frame, textColor: .standartWhite, fontName: .gilroyMedium, fontSize: 16, textAlignment: .right, numberOfLines: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var genderLabel: UILabel = {
        let frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        let label = CustomLable(frame: frame, textColor: .standartWhite, fontName: .gilroyMedium, fontSize: 16, textAlignment: .right, numberOfLines: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // MARK: - StackView in StackViewContainer
    private lazy var speciesStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.spacing = 8
        [titleSpeciesLabel, speciesLabel].forEach { stack.addArrangedSubview($0) }
        return stack
    }()
    
    private lazy var typeStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.spacing = 8
        [titleTypeLabel, typeLabel].forEach { stack.addArrangedSubview($0) }
        return stack
    }()
    
    private lazy var genderStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.spacing = 8
        [titleGenderLabel, genderLabel].forEach { stack.addArrangedSubview($0) }
        return stack
    }()
    
    // MARK: - StackViewContainer
    private lazy var stackViewContainer: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.spacing = 16
        [speciesStackView, typeStackView, genderStackView].forEach { stack.addArrangedSubview($0)}
        return stack
    }()
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupCellView()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Override Methods
    override func prepareForReuse() {
        speciesLabel.text = nil
        typeLabel.text = nil
        genderLabel.text = nil
    }
    
    // MARK: - Public Methods
    func configure(with cellData: Information) {
        indicator.startAnimating()
        checkData = cellData
        queue.async {
            guard self.checkData == cellData else {
                return }
            DispatchQueue.main.async {
                self.indicator.stopAnimating()
                self.speciesLabel.text = cellData.species
                self.genderLabel.text = cellData.gender
                
                if cellData.type.isEmpty {
                    self.typeLabel.text = "None"
                    
                } else {
                    self.typeLabel.text = cellData.type
                }
                
            }
        }
    }
    
    // MARK: - Private Methods
    private func setupConstraints() {
        indicator.translatesAutoresizingMaskIntoConstraints = false
        addSubview(indicator)
          NSLayoutConstraint.activate([
            indicator.centerXAnchor.constraint(equalTo: centerXAnchor),
            indicator.centerYAnchor.constraint(equalTo: centerYAnchor),
             ])
        
        stackViewContainer.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stackViewContainer)
        NSLayoutConstraint.activate([
            stackViewContainer.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            stackViewContainer.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            stackViewContainer.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            stackViewContainer.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16)
        ])
    }
    
    private func setupCellView() {
        contentView.layer.cornerRadius = 10
        contentView.backgroundColor = #colorLiteral(red: 0.1490196078, green: 0.1647058824, blue: 0.2196078431, alpha: 1)
    }
}
