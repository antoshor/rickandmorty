//
//  EpisodesCollectionViewCell.swift
//  Rick&Morty
//
//  Created by Mac Admin on 19.08.2023.
//

import Foundation
import UIKit

final class EpisodesCollectionViewCell: UICollectionViewCell, CollectionViewCellsProtocol {
    
    // MARK: - Protocol implementation
    static var reuseID: String = "EpisodesCollectionViewCell"
    typealias CellType = Episode
    
    // MARK: - Private Properties
    private let queue = DispatchQueue(label: "queue.cell")
    private var checkData: Episode?
    
    // MARK: - UI Properties
    private lazy var indicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .medium)
        indicator.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        indicator.color = .white
        indicator.hidesWhenStopped = true
        return indicator
    }()
    
    // MARK: - Labels
    private lazy var titleLabel: UILabel = {
        let frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        let label = CustomLable(frame: frame, textColor: .standartWhite, fontName: .gilroySemiBold, fontSize: 17, textAlignment: .left, numberOfLines: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var episodeSeasonLabel: UILabel = {
        let frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        let label = CustomLable(frame: frame, textColor: .green, fontName: .gilroyMedium, fontSize: 13, textAlignment: .left, numberOfLines: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var dateLabel: UILabel = {
        let frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        let label = CustomLable(frame: frame, textColor: .gray, fontName: .gilroyMedium, fontSize: 12, textAlignment: .right, numberOfLines: 1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // MARK: - LabelsStackView
    private lazy var labelsStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.spacing = 8
        [episodeSeasonLabel,dateLabel].forEach { stack.addArrangedSubview($0) }
        return stack
    }()
    
    // MARK: - StackViewContainer
    private lazy var stackViewContainer: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 16
        [titleLabel, labelsStackView].forEach { stack.addArrangedSubview($0)}
        return stack
    }()
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupCellView()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Override Methods
    override func prepareForReuse() {
        titleLabel.text = nil
        episodeSeasonLabel.text = nil
        dateLabel.text = nil
    }
    
    // MARK: - Public Methods
    func configure(with cellData: Episode) {
        indicator.startAnimating()
        checkData = cellData
        queue.async {
            guard self.checkData == cellData else {
                return }
            DispatchQueue.main.async {
                self.indicator.stopAnimating()
                self.titleLabel.text = cellData.name
                self.episodeSeasonLabel.text = cellData.episode
                self.dateLabel.text = cellData.airDate
            }
        }
    }
    
    // MARK: - Private Methods
    func setupConstraints() {
        indicator.translatesAutoresizingMaskIntoConstraints = false
        addSubview(indicator)
          NSLayoutConstraint.activate([
            indicator.centerXAnchor.constraint(equalTo: centerXAnchor),
            indicator.centerYAnchor.constraint(equalTo: centerYAnchor),
             ])
        
        stackViewContainer.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stackViewContainer)
        NSLayoutConstraint.activate([
            stackViewContainer.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            stackViewContainer.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            stackViewContainer.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            stackViewContainer.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16)
        ])
    }
    
    private func setupCellView() {
        contentView.layer.cornerRadius = 10
        contentView.clipsToBounds = true
        contentView.backgroundColor = #colorLiteral(red: 0.1490196078, green: 0.1647058824, blue: 0.2196078431, alpha: 1)
    }
}
