//
//  ProfileCollectionViewCell.swift
//  Rick&Morty
//
//  Created by Mac Admin on 18.08.2023.
//

import Foundation
import UIKit

final class ProfileCollectionViewCell: UICollectionViewCell, CollectionViewCellsProtocol {
    
    // MARK: - Protocol implementation
    static var reuseID: String = "ProfileCollectionViewCell"
    typealias CellType = Profile
    
    // MARK: - Private Properties
    private let queue = DispatchQueue(label: "queue.cell")
    private var checkData: Profile?
    
    // MARK: - UI Properties
    private lazy var indicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .medium)
        indicator.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        indicator.color = .white
        indicator.hidesWhenStopped = true
        return indicator
    }()
    
    // MARK: - ImageView
    private lazy var imageView: UIImageView = {
        let image = UIImageView()
        image.layer.cornerRadius = 10
        image.clipsToBounds = true
        return image
    }()
    
    // MARK: - Labels
    private lazy var titleLabel: UILabel = {
        let frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        let label = CustomLable(frame: frame, textColor: .standartWhite, fontName: .gilroyBold, fontSize: 22, textAlignment: .center, numberOfLines: 0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var statusLabel: UILabel = {
        let frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        let label = CustomLable(frame: frame, textColor: .green, fontName: .gilroyMedium, fontSize: 16, textAlignment: .center, numberOfLines: 0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // MARK: - LabelsStackView
    private lazy var labelsStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 8
        [titleLabel, statusLabel].forEach { stack.addArrangedSubview($0) }
        return stack
    }()
    
    // MARK: - StackViewContainer
    private lazy var stackViewContainer: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 24
        [imageView, labelsStackView].forEach { stack.addArrangedSubview($0) }
        return stack
    }()
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupCellView()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Override Methods
    override func prepareForReuse() {
        imageView.image = nil
        titleLabel.text = nil
    }
    
    // MARK: - Public Methods
    func configure(with cellData: Profile) {
        indicator.startAnimating()
        checkData = cellData
        queue.async {
            guard let url = URL(string:"\(cellData.image)") else { return }
            if let data = try? Data(contentsOf: url),
               self.checkData == cellData
            {
                DispatchQueue.main.async {
                    self.indicator.stopAnimating()
                    self.imageView.image = UIImage(data: data)
                    self.titleLabel.text = cellData.name
                    self.statusLabel.text = cellData.status
                    self.statusLabel.textColor = StatusTextColor(rawValue: cellData.status)?.textColor
                }
            }
        }
    }
    
    // MARK: - Private Methods
    func setupConstraints() {
        indicator.translatesAutoresizingMaskIntoConstraints = false
        addSubview(indicator)
        NSLayoutConstraint.activate([
            indicator.centerXAnchor.constraint(equalTo: centerXAnchor),
            indicator.centerYAnchor.constraint(equalTo: centerYAnchor),
        ])
        
        stackViewContainer.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stackViewContainer)
        NSLayoutConstraint.activate([
            stackViewContainer.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            stackViewContainer.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0),
            
            stackViewContainer.centerXAnchor.constraint(equalTo: centerXAnchor),
            stackViewContainer.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor, multiplier: 1)
        ])
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleLabel.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.134)
        ])
        
        statusLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            statusLabel.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.08)
        ])
    }
    
    private func setupCellView() {
        contentView.layer.cornerRadius = 10
        contentView.clipsToBounds = true
        contentView.backgroundColor = #colorLiteral(red: 0.01568627451, green: 0.04705882353, blue: 0.1176470588, alpha: 1)
    }
}

// MARK: - StatusTextColor
enum StatusTextColor: String {
    case dead = "Dead"
    case alive = "Alive"
    case unknow =  "unknown"
    
    var textColor: UIColor {
        switch self {
        case .dead:
            return TextColor.standartRed.textColor
        case .alive:
            return TextColor.green.textColor
        case .unknow:
            return TextColor.standartWhite.textColor
        }
    }
}
