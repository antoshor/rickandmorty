//
//  CustomLable + Enum.swift
//  Rick&Morty
//
//  Created by Mac Admin on 22.08.2023.
//

import UIKit

final class CustomLable: UILabel {
    
    // MARK: - Initializers
    init(frame: CGRect, textColor: TextColor, fontName: Fonts, fontSize: CGFloat, textAlignment: NSTextAlignment, numberOfLines: Int) {
        super.init(frame: frame)
        setup(textColor: textColor, fontName: fontName, fontSize: fontSize, textAlignment: textAlignment, numberOfLines: numberOfLines)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    // MARK: - Private Methods
    private func setup(textColor: TextColor, fontName: Fonts, fontSize: CGFloat, textAlignment: NSTextAlignment, numberOfLines: Int) {
        self.textColor = textColor.textColor
        self.font = UIFont(name: fontName.rawValue, size: fontSize)
        self.textAlignment = textAlignment
        self.numberOfLines = numberOfLines
        adjustsFontSizeToFitWidth = true
    }
}

// MARK: - TextColor
enum TextColor {
    case standartWhite
    case green
    case lightGray
    case gray
    case standartRed
    
    var textColor: UIColor {
        switch self {
        case .standartWhite:
            return .white
        case .green:
            return #colorLiteral(red: 0.3242046535, green: 0.799691081, blue: 0.03117593378, alpha: 1)
        case .lightGray:
            return #colorLiteral(red: 0.768627451, green: 0.7882352941, blue: 0.8078431373, alpha: 1)
        case .gray:
            return #colorLiteral(red: 0.6425282955, green: 0.6608385444, blue: 0.67485708, alpha: 1)
        case .standartRed:
            return .red
        }
        
    }
}

// MARK: - Fonts
enum Fonts: String {
    case gilroyBold = "Gilroy-Bold"
    case gilroySemiBold = "Gilroy-SemiBold"
    case gilroyMedium = "Gilroy-Medium"
}
