//
//  DetailRouter.swift
//  Rick&Morty
//
//  Created by Mac Admin on 17.08.2023.
//

import Foundation

protocol DetailRouterProtocol {
    
}

final class DetailRouter {
    weak var presenter: DetailPresenterProtocol?
    weak var viewController: DetailViewController?
}

extension DetailRouter: DetailRouterProtocol {
    
}
