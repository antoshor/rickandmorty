//
//  NetworkService.swift
//  Rick&Morty
//
//  Created by Mac Admin on 17.08.2023.
//

import Foundation

final class NetworkService {
    
    // MARK: - Static Properties
    static let shared = NetworkService()
    
    // MARK: - Public Properties
    func loadData<T: Codable>(url: String, completion: @escaping (Result<T, LoadingError>) ->()) {
        
        guard let url = URL(string: url) else {
            completion(.failure(.urlFailure))
            return }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
                completion(.failure(.networkFailure(error)))
            } else if let data = data {
                guard let jsonData = try? JSONDecoder().decode(T.self, from: data) else {
                    print("Error JSONDecoder()")
                    completion(.failure(.jsonDecodeFailure))
                    return }
                DispatchQueue.main.async {
                    completion(.success(jsonData))
                }
            }
        }.resume()
    }
}

// MARK: - LoadingError
enum LoadingError: Error {
    case networkFailure(Error)
    case urlFailure
    case jsonDecodeFailure
}
