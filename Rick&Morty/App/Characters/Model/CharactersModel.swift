//
//  CharactersModel.swift
//  Rick&Morty
//
//  Created by Mac Admin on 17.08.2023.
//

import Foundation

// MARK: - CharacterModel
struct CharacterModel: Codable {
    let info: Info
    let results: [Results]
}

// MARK: - Info
struct Info: Codable {
    let count, pages: Int
    let next: String
}

// MARK: - Results
struct Results: Codable, Hashable {
    let id: Int
    let name: String
    let status: Status
    let species: Species
    let type: String
    let gender: Gender
    let origin, location: Location
    let image: String
    let episode: [String]
    let url: String
    let created: String
}

enum Gender: String, Codable, Hashable {
    case female = "Female"
    case male = "Male"
    case unknown = "unknown"
}

// MARK: - Location
struct Location: Codable, Hashable {
    let name: String
    let url: String
}

enum Species: String, Codable, Hashable {
    case alien = "Alien"
    case human = "Human"
}

enum Status: String, Codable, Hashable {
    case alive = "Alive"
    case dead = "Dead"
    case unknown = "unknown"
}
