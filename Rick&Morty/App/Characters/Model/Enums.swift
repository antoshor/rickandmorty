//
//  Enums.swift
//  Rick&Morty
//
//  Created by Mac Admin on 17.08.2023.
//

import Foundation

// MARK: - CharactersDifableDataSource
enum CharactersSection: Int, CaseIterable {
    case characters
}

enum CharactersRow: Hashable {
    case charactersInfo(Results)
}

