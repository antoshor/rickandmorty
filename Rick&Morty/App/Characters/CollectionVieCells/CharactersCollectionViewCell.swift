//
//  CharactersCollectionViewCell.swift
//  Rick&Morty
//
//  Created by Mac Admin on 17.08.2023.
//

import Foundation
import UIKit

final class CharactersCollectionViewCell: UICollectionViewCell, CollectionViewCellsProtocol {
    
    // MARK: - Protocol implementation
    static var reuseID: String = "CharactersCollectionViewCell"
    typealias CellType = Results
    
    // MARK: - Private Properties
    private let queue = DispatchQueue(label: "queue.cell")
    private var checkData: Results?
    
   // MARK: - UI Properties
    private lazy var indicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .medium)
        indicator.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        indicator.color = .white
        indicator.hidesWhenStopped = true
        return indicator
    }()
    
    // MARK: - ImageView
    private lazy var imageView: UIImageView = {
        let image = UIImageView()
        // image.contentMode = .scaleAspectFill
        image.sizeToFit()
        image.layer.cornerRadius = 10
        image.clipsToBounds = true
        return image
    }()
    
    // MARK: - Labels
    private lazy var titleLabel: UILabel = {
        let frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        let label = CustomLable(frame: frame, textColor: .standartWhite, fontName: .gilroySemiBold, fontSize: 17, textAlignment: .center, numberOfLines: 0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // MARK: - StackViewContainer
    private lazy var stackViewContainer: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 16
        [imageView, titleLabel].forEach { stack.addArrangedSubview($0) }
        return stack
    }()
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        //super.awakeFromNib()
        setupCellView()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        imageView.image = nil
        titleLabel.text = nil
    }
    
    // MARK: - Public Methods
    func configure(with cellData: Results) {
        indicator.startAnimating()
        checkData = cellData
        queue.async {
            guard let url = URL(string:"\(cellData.image)") else { return }
            if let data = try? Data(contentsOf: url),
               self.checkData == cellData
            {
                DispatchQueue.main.async {
                    self.indicator.stopAnimating()
                    self.imageView.image = UIImage(data: data)
                    self.titleLabel.text = cellData.name
                }
            }
        }
    }
    
    // MARK: - Private Methods
    private func setupConstraints() {
        indicator.translatesAutoresizingMaskIntoConstraints = false
        addSubview(indicator)
        NSLayoutConstraint.activate([
            indicator.centerXAnchor.constraint(equalTo: centerXAnchor),
            indicator.centerYAnchor.constraint(equalTo: centerYAnchor),
        ])
        
        stackViewContainer.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stackViewContainer)
        NSLayoutConstraint.activate([
            stackViewContainer.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            stackViewContainer.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            stackViewContainer.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            stackViewContainer.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16)
        ])
        
        let imageWidth =  imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor, multiplier: 1)
        imageWidth.priority = UILayoutPriority(rawValue: 750)
        imageWidth.isActive = true
        
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleLabel.widthAnchor.constraint(equalTo: imageView.widthAnchor, multiplier: 1),
            titleLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 20)
        ])
    }
    
    private func setupCellView() {
        contentView.layer.cornerRadius = 10
        contentView.clipsToBounds = true
        contentView.backgroundColor = #colorLiteral(red: 0.1490196078, green: 0.1647058824, blue: 0.2196078431, alpha: 1)
    }
}
