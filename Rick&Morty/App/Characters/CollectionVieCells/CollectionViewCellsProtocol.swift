//
//  CollectionViewCellsProtocol.swift
//  Rick&Morty
//
//  Created by Mac Admin on 22.08.2023.
//

import Foundation

protocol CollectionViewCellsProtocol {
    
    associatedtype CellType
    
    static var reuseID: String { get }
    
    func configure(with: CellType)
}
