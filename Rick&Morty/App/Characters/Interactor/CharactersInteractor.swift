//
//  CharactersInteractor.swift
//  Rick&Morty
//
//  Created by Mac Admin on 17.08.2023.
//

import Foundation

final class CharactersInteractor {
    
    // MARK: - Public Properties
    weak var presenter: CharactersPresenterProtocol?
    var data: CharacterModel?
}

extension CharactersInteractor: CharactersInteractorProtocol {
    func loadData() {
        NetworkService.shared.loadData(url: Api.characterApi.rawValue) { [weak self] (result: Result<CharacterModel, LoadingError>) in
            switch result {
            case .success(let data):
                self?.data = data
                self?.presenter?.didLoadData(data)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}

 // MARK: - Api
enum Api: String {
    case characterApi = "https://rickandmortyapi.com/api/character"
}
