//
//  CharactersInteractorProtocol.swift
//  Rick&Morty
//
//  Created by Mac Admin on 17.08.2023.
//

import Foundation

protocol CharactersInteractorProtocol: AnyObject {
    
    func loadData()
    
    var data: CharacterModel? { get set }
}
