//
//  CharactersViewProtocol.swift
//  Rick&Morty
//
//  Created by Mac Admin on 17.08.2023.
//

import Foundation
import UIKit

protocol CharactersViewProtocol: AnyObject {
    
    func showUI(snapshot: NSDiffableDataSourceSnapshot<CharactersSection, CharactersRow>)
}
