//
//  CharactersViewController.swift
//  Rick&Morty
//
//  Created by Mac Admin on 17.08.2023.
//

import UIKit

final class CharactersViewController: UIViewController, CharactersViewProtocol {

    // MARK: - Public Properties
    var presenter: CharactersPresenterProtocol?
    var collectionViewDataSource: CharactersDiffableDataSourceProtocol?
    
    // MARK: - Private Properties
    private lazy var charactersCollectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: 100, height: 100), collectionViewLayout: CharactersCompositionalLayout.createLayuot())
        collectionView.backgroundColor = #colorLiteral(red: 0.01568627451, green: 0.04705882353, blue: 0.1176470588, alpha: 1)
        collectionView.delegate = self
        collectionView.register(CharactersCollectionViewCell.self, forCellWithReuseIdentifier: CharactersCollectionViewCell.reuseID)
        return collectionView
    }()
    
    // MARK: - Initializers
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = #colorLiteral(red: 0.01568627451, green: 0.04705882353, blue: 0.1176470588, alpha: 1)
        presenter?.initialLoad()
        setupDataSource()
        setupNavVC()
        setupConstraints()
    }

    // MARK: - Public Methods
    func showUI(snapshot: NSDiffableDataSourceSnapshot<CharactersSection, CharactersRow>) {
        collectionViewDataSource?.dataSource?.apply(snapshot)
    }
    
    // MARK: - Private Methods
    private func setupConstraints() {
        charactersCollectionView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(charactersCollectionView)
        let safeArea = view.safeAreaLayoutGuide
          NSLayoutConstraint.activate([
            charactersCollectionView.topAnchor.constraint(equalTo: view.topAnchor),
            charactersCollectionView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            charactersCollectionView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),
            charactersCollectionView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor)
        ])
    }
    
    private func setupDataSource() {
        collectionViewDataSource = CharactersDiffableDataSource(collectionView: charactersCollectionView)
    }
    
    private func setupNavVC() {
       // self.navigationController?.hidesBarsOnSwipe = true
        navigationController?.navigationBar.prefersLargeTitles = true
        title = "Characters"
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationItem.backButtonTitle = ""
        navigationController?.navigationBar.tintColor = .white
    }
}

extension CharactersViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter?.didTapToCategory(to: indexPath.row)
    }
}

