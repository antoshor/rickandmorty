//
//  CharactersDiffableDataSource.swift
//  Rick&Morty
//
//  Created by Mac Admin on 17.08.2023.
//

import Foundation
import UIKit

// MARK: - DataSource
protocol CharactersDiffableDataSourceProtocol {
    init(collectionView: UICollectionView)
    var dataSource: UICollectionViewDiffableDataSource<CharactersSection, CharactersRow>? { get }
}

// MARK: - MainDataSource
final class CharactersDiffableDataSource: CharactersDiffableDataSourceProtocol {
    
    // MARK: - Public Properties
    var dataSource: UICollectionViewDiffableDataSource<CharactersSection, CharactersRow>?
    
    // MARK: - Initializers
    required init(collectionView: UICollectionView) {
        configureDataSource(collectionView: collectionView)
    }
    
    // MARK: - Private Methods
    private func configureDataSource(collectionView: UICollectionView) {
        dataSource = UICollectionViewDiffableDataSource<CharactersSection, CharactersRow>(collectionView: collectionView, cellProvider: { (collectionView, indexPath, item) -> UICollectionViewCell? in
            switch item {
            case .charactersInfo(let info):
                 let cell = self.createCell(cellType: CharactersCollectionViewCell.self, indexPath: indexPath, collectionView: collectionView)
                cell.configure(with: info)
                return cell
            }
        })
    }
    
    private func createCell<T: CollectionViewCellsProtocol>(cellType: T.Type, indexPath: IndexPath, collectionView: UICollectionView) -> T {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellType.reuseID, for: indexPath) as? T else {
            fatalError("Error \(cellType)")
        }
        return cell
    }
}
