//
//  CharactersCompositionalLayout.swift
//  Rick&Morty
//
//  Created by Mac Admin on 17.08.2023.
//

import Foundation
import UIKit

// MARK: - CompositionalLayoutProtocol
protocol CompositionalLayoutProtocol {
    
    static func createLayuot() -> UICollectionViewLayout
}

final class CharactersCompositionalLayout: CompositionalLayoutProtocol {
    
    static func createLayuot() -> UICollectionViewLayout {
        
        let layout = UICollectionViewCompositionalLayout  { (sectionIndex, layoutEnvironment) -> NSCollectionLayoutSection? in
         
            guard let sectionKind = CharactersSection(rawValue: sectionIndex) else {
                return nil
            }
            
            switch sectionKind {
            case .characters:
                let spacing: CGFloat = 20
                
                let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.5), heightDimension: .fractionalHeight(1.0))
                let item = NSCollectionLayoutItem(layoutSize: itemSize)
              
                let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalWidth(0.56))
                let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: 2)
                group.interItemSpacing = .fixed(16)
                
                let section = NSCollectionLayoutSection(group: group)
                section.interGroupSpacing = 16

                section.contentInsets = .init(top: spacing, leading: spacing, bottom: spacing, trailing: spacing)
                
                return section
            }
        }
        
        return layout
    }
}
