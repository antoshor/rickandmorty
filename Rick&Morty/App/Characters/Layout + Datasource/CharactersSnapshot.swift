//
//  CharactersSnapshot.swift
//  Rick&Morty
//
//  Created by Mac Admin on 17.08.2023.
//

import Foundation
import UIKit

protocol CharactersSnapshotProtocol {
    associatedtype Section: Hashable
    associatedtype Row: Hashable
    associatedtype DiffableData
    
    func reloadData(with data: DiffableData) -> NSDiffableDataSourceSnapshot<Section, Row>
}

final class CharactersSnapshot: CharactersSnapshotProtocol {
    
    typealias DiffableData = CharacterModel
    typealias Section = CharactersSection
    typealias Row = CharactersRow
    
    func reloadData(with data: CharacterModel ) -> NSDiffableDataSourceSnapshot<CharactersSection, CharactersRow> {
        var snapshot = NSDiffableDataSourceSnapshot<CharactersSection, CharactersRow>()
        snapshot.appendSections([.characters])
        data.results.forEach { item in
            snapshot.appendItems([.charactersInfo(item)], toSection: .characters)
        }
        return snapshot
    }
}
