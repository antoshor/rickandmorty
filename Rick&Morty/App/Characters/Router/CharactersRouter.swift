//
//  CharactersRouter.swift
//  Rick&Morty
//
//  Created by Mac Admin on 17.08.2023.
//

import Foundation

final class CharactersRouter {
    
    // MARK: - Public Properties
    weak var presenter: CharactersPresenterProtocol?
    weak var view: CharactersViewController?
}

extension CharactersRouter: CharactersRouterProtocol {
    func showDetailVC(with info: DetailModel) {
        let vc = DetailModuleBuilder.build(info: info)
        view?.navigationController?.pushViewController(vc, animated: true)
    }
}
