//
//  CharactersRouterProtocol.swift
//  Rick&Morty
//
//  Created by Mac Admin on 17.08.2023.
//

import Foundation

protocol CharactersRouterProtocol: AnyObject {
    
    func showDetailVC(with info: DetailModel)
}
