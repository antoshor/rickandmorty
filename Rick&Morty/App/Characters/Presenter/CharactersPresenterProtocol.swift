//
//  CharactersPresenterProtocol.swift
//  Rick&Morty
//
//  Created by Mac Admin on 17.08.2023.
//

import Foundation

protocol CharactersPresenterProtocol: AnyObject {
 
    func initialLoad()
    
    func didLoadData(_ data: CharacterModel)
    
    func didTapToCategory(to number: Int)
}
