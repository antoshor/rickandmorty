//
//  CharactersPresenter.swift
//  Rick&Morty
//
//  Created by Mac Admin on 17.08.2023.
//

import Foundation

final class CharactersPresenter {
    
    // MARK: - Public Properties
    weak var view: CharactersViewProtocol? //вот тут он не добавляет его в инит
    var interactor: CharactersInteractorProtocol
    var router: CharactersRouterProtocol
    var snapshotDelegate: CharactersSnapshot?
    
    // MARK: - Initializers
    init(interactor: CharactersInteractorProtocol, router: CharactersRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
}

extension CharactersPresenter: CharactersPresenterProtocol {
    
    func initialLoad() {
        interactor.loadData()
    }
    
    func didLoadData(_ data: CharacterModel) {
        guard let snapshot = snapshotDelegate?.reloadData(with: data) else {
            return
        }
        view?.showUI(snapshot: snapshot)
    }
    
    func didTapToCategory(to number: Int) {
        guard let info = interactor.data?.results[number] else {
            print("Error category number \(number) not found")
            return
        }
        
        let detail = DetailModel(name: info.name, status: info.status.rawValue, species: info.species.rawValue, type: info.type, gender: info.gender.rawValue, image: info.image, originUrl: info.origin.url, episodeUrl: info.episode)
        
        router.showDetailVC(with: detail)
    }
    
}
