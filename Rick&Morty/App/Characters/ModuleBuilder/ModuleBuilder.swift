//
//  CharactersModuleBuilder.swift
//  Rick&Morty
//
//  Created by Mac Admin on 17.08.2023.
//

import Foundation
import UIKit

final class CharactersModuleBuilder {
    static func build() -> CharactersViewController {
        let interactor = CharactersInteractor()
        let router = CharactersRouter()
        
        let viewController = CharactersViewController()
        let presenter = CharactersPresenter(interactor: interactor, router: router)
        presenter.snapshotDelegate = CharactersSnapshot()
        presenter.view = viewController
        
        interactor.presenter = presenter
        
        router.presenter = presenter
        router.view = viewController
        
        viewController.presenter = presenter
                
        return viewController
    }
}
